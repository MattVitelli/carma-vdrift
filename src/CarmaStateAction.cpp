//
//  CarmaStateAction.cpp
//  vdrift
//
//  Created by Matt Vitelli on 2/2/16.
//  Copyright © 2016 VDrift Team. All rights reserved.
//

#include "CarmaStateAction.hpp"

CarmaState::CarmaState()
{
	m_simulationState = SimulationState::LOADING; //Default value
}

CarmaState::~CarmaState()
{
	
}

CarmaAction::CarmaAction()
{
	m_requestReset = false; //Default value
	m_steering = 0;
	m_acceleration = 0;
}

CarmaAction::~CarmaAction()
{
}
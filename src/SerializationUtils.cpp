//
//  SerializationUtils.cpp
//  vdrift
//
//  Created by Matt Vitelli on 2/2/16.
//  Copyright © 2016 VDrift Team. All rights reserved.
//

#include "SerializationUtils.hpp"

libcarma::SerializedMatrix_SerializedMatrixType OpenCVToSerializedMatrixType(const cv::Mat &inMat)
{
	libcarma::SerializedMatrix_SerializedMatrixType type;
	switch (inMat.type()) {
		case CV_8UC1:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_BYTE;
			break;
		case CV_8UC2:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_BYTE;
			break;
		case CV_8UC3:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_BYTE;
			break;
		case CV_8UC4:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_BYTE;
			break;
		case CV_8SC1:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_BYTE;
			break;
		case CV_8SC2:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_BYTE;
			break;
		case CV_8SC3:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_BYTE;
			break;
		case CV_8SC4:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_BYTE;
			break;
		case CV_16UC1:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_SHORT;
			break;
		case CV_16UC2:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_SHORT;
			break;
		case CV_16UC3:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_SHORT;
			break;
		case CV_16UC4:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_SHORT;
			break;
		case CV_16SC1:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_SHORT;
			break;
		case CV_16SC2:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_SHORT;
			break;
		case CV_16SC3:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_SHORT;
			break;
		case CV_16SC4:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_SHORT;
			break;
		case CV_32FC1:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_FLOAT;
			break;
		case CV_32FC2:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_FLOAT;
			break;
		case CV_32FC3:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_FLOAT;
			break;
		case CV_32FC4:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_FLOAT;
			break;
		case CV_64FC1:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_DOUBLE;
			break;
		case CV_64FC2:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_DOUBLE;
			break;
		case CV_64FC3:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_DOUBLE;
			break;
		case CV_64FC4:
			type = libcarma::SerializedMatrix_SerializedMatrixType_SMT_DOUBLE;
			break;
	}
	return type;
}

int SerializedMatrixTypeToOpenCV(const libcarma::SerializedMatrix *inMat)
{
	int type = -1;
	switch (inMat->matrix_datatype()) {
		case libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_BYTE:
			switch (inMat->channels()) {
				case 1:
					type = CV_8UC1;
					break;
				case 2:
					type = CV_8UC2;
					break;
				case 3:
					type = CV_8UC3;
					break;
				case 4:
					type = CV_8UC4;
					break;
			}
			break;
		case libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_BYTE:
			switch (inMat->channels()) {
				case 1:
					type = CV_8SC1;
					break;
				case 2:
					type = CV_8SC2;
					break;
				case 3:
					type = CV_8SC3;
					break;
				case 4:
					type = CV_8SC4;
					break;
			}
			break;
		case libcarma::SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_SHORT:
			switch (inMat->channels()) {
				case 1:
					type = CV_16UC1;
					break;
				case 2:
					type = CV_16UC2;
					break;
				case 3:
					type = CV_16UC3;
					break;
				case 4:
					type = CV_16UC4;
					break;
			}
			break;
		case libcarma::SerializedMatrix_SerializedMatrixType_SMT_SIGNED_SHORT:
			switch (inMat->channels()) {
				case 1:
					type = CV_16SC1;
					break;
				case 2:
					type = CV_16SC2;
					break;
				case 3:
					type = CV_16SC3;
					break;
				case 4:
					type = CV_16SC4;
					break;
			}
			break;
			/*
			 case SerializedMatrix_SerializedMatrixType_SMT_UNSIGNED_INT:
			 switch (inMat->channels()) {
			 case 1:
			 type = CV_32UC1;
			 break;
			 case 2:
			 type = CV_32UC2;
			 break;
			 case 3:
			 type = CV_32UC3;
			 break;
			 case 4:
			 type = CV_32UC4;
			 break;
			 }
			 break;
			 case SerializedMatrix_SerializedMatrixType_SMT_SIGNED_INT:
			 switch (inMat->channels()) {
			 case 1:
			 type = CV_32SC1;
			 break;
			 case 2:
			 type = CV_32SC2;
			 break;
			 case 3:
			 type = CV_32SC3;
			 break;
			 case 4:
			 type = CV_32SC4;
			 break;
			 }
			 break;
			 */
		case libcarma::SerializedMatrix_SerializedMatrixType_SMT_FLOAT:
			switch (inMat->channels()) {
				case 1:
					type = CV_32FC1;
					break;
				case 2:
					type = CV_32FC2;
					break;
				case 3:
					type = CV_32FC3;
					break;
				case 4:
					type = CV_32FC4;
					break;
			}
			break;
		case libcarma::SerializedMatrix_SerializedMatrixType_SMT_DOUBLE:
			switch (inMat->channels()) {
				case 1:
					type = CV_64FC1;
					break;
				case 2:
					type = CV_64FC2;
					break;
				case 3:
					type = CV_64FC3;
					break;
				case 4:
					type = CV_64FC4;
					break;
			}
			break;
	}
	return type;
}

void SerializeMatrix(const cv::Mat &inMat, libcarma::SerializedMatrix *outMat)
{
	outMat->set_width(inMat.cols);
	outMat->set_height(inMat.rows);
	outMat->set_channels(inMat.channels());
	outMat->set_matrix_datatype(OpenCVToSerializedMatrixType(inMat));
	outMat->add_pixel_data(inMat.data, (inMat.dataend - inMat.datastart));
}

void DeserializeMatrix(const libcarma::SerializedMatrix *inMat, cv::Mat &outMat)
{
	int width = inMat->width();
	int height = inMat->height();
	int format = SerializedMatrixTypeToOpenCV(inMat);
	cv::Mat newMat = cv::Mat(height, width, format);
	newMat.data = (uchar *)inMat->pixel_data(0).c_str();
	newMat.copyTo(outMat);
}

void SerializeVector(const Vec3 &input, libcarma::SerializedVector3 *output)
{
	if(!output)
		return;
	
	output->set_v_x(input[0]);
	output->set_v_y(input[1]);
	output->set_v_z(input[2]);
}

void DeserializeVector(const libcarma::SerializedVector3 *input, Vec3 &output)
{
	if(!input)
		return;
	
	output[0] = input->v_x();
	output[1] = input->v_y();
	output[2] = input->v_z();
}

void DeserializeAction(const libcarma::CarmaAction *input, CarmaAction &output)
{
	if(!input)
		return;
	
	output.m_requestReset = input->requestreset();
	output.m_acceleration = input->acceleration();
	output.m_steering = input->steering();
}

libcarma::CarmaState_SimulationStateType SimulationStateToSerializedSimulationState(SimulationState state)
{
	libcarma::CarmaState_SimulationStateType type = libcarma::CarmaState_SimulationStateType::CarmaState_SimulationStateType_SIMULATION_LOADING;
	switch (state) {
  case SimulationState::STARTED:
			type = libcarma::CarmaState_SimulationStateType::CarmaState_SimulationStateType_SIMULATION_STARTED;
			break;
  case SimulationState::RUNNING:
			type = libcarma::CarmaState_SimulationStateType::CarmaState_SimulationStateType_SIMULATION_RUNNING;
			break;
  case SimulationState::ENDED:
			type = libcarma::CarmaState_SimulationStateType::CarmaState_SimulationStateType_SIMULATION_ENDED;
			break;
  case SimulationState::LOADING:
			type = libcarma::CarmaState_SimulationStateType::CarmaState_SimulationStateType_SIMULATION_LOADING;
			break;
	}
	
	return type;
}

void SerializeState(const CarmaState &input, libcarma::CarmaState *output)
{
	if(!output)
		return;
	
	output->set_state_type(SimulationStateToSerializedSimulationState(input.m_simulationState));
	SerializeMatrix(input.m_image, output->mutable_state_image());
	SerializeVector(input.m_linearVelocity, output->mutable_state_linear_velocity());
	SerializeVector(input.m_linearAcceleration, output->mutable_state_linear_acceleration());
	SerializeVector(input.m_angularVelocity, output->mutable_state_angular_velocity());
	SerializeVector(input.m_angularAcceleration, output->mutable_state_angular_acceleration());
}

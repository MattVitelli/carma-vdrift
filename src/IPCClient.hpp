//
//  IPCClient.hpp
//  vdrift
//
//  Created by Matt Vitelli on 2/2/16.
//  Copyright © 2016 VDrift Team. All rights reserved.
//

#ifndef IPCClient_hpp
#define IPCClient_hpp

/*
//Boost 1.6 hotfix
#ifndef BOOST_RV_REF_BEG_IF_CXX11
#define BOOST_RV_REF_BEG_IF_CXX11
#endif
*/
 
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <memory>
#include <thread>
//#include <boost/thread.hpp>
#include "CarmaStateAction.hpp"

class IPCClient
{
public:
	IPCClient();
	~IPCClient();
	void updateState(const CarmaState &state);
	CarmaAction getAction();
	
	void init();
	void shutdown();
	
private:
	static void IPCStateLoop(IPCClient *client);
	static void IPCActionLoop(IPCClient *client);
	//static void IPCLoop(IPCClient *client);
	std::mutex		m_stateLock;
	std::mutex		m_actionLock;
	CarmaState			m_state;
	CarmaAction			m_action;
	int							m_socket;
	bool						m_isRunning;
	bool						m_hasNewState;
	std::thread * m_threadState;
	std::thread * m_threadAction;
};

#endif /* IPCClient_hpp */

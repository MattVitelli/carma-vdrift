//
//  IPCClient.cpp
//  vdrift
//
//  Created by Matt Vitelli on 2/2/16.
//  Copyright © 2016 VDrift Team. All rights reserved.
//

#include "IPCClient.hpp"
#include <nanomsg/nn.h>
#include <nanomsg/pair.h>
#include "SerializationUtils.hpp"

IPCClient::IPCClient()
{
	m_threadState = nullptr;
	m_threadAction = nullptr;
	m_hasNewState = false;
	m_isRunning = false;
}

IPCClient::~IPCClient()
{
	
}

void IPCClient::init()
{
	if(m_isRunning)
		return;
	
	const char *ipc_url = "ipc:///tmp/carma.ipc";
	int sock = nn_socket (AF_SP, NN_PAIR);
	bool success = (sock >= 0);
	assert(success);
	success = (nn_bind (sock, ipc_url) >= 0);
	assert(success);
	m_socket = sock;
	m_isRunning = true;
	m_threadState = new std::thread(IPCStateLoop, this);
	m_threadAction = new std::thread(IPCActionLoop, this);
}

void IPCClient::shutdown()
{
	if(!m_isRunning)
		return;
	
	m_isRunning = false;
	/*
	 * Technically, this is not the right thing to do,
	 * but I don't know of a better way to kill the
	 * thread when its stuck in a nn_recv call
	 */
	/*
	if(m_threadState != nullptr)
	{
		m_threadState->join();
		m_threadState = nullptr;
	}
	*/
	/*
	if(m_threadAction != nullptr)
	{
		m_threadAction->join();
		m_threadAction = nullptr;
	}
	*/
	
	nn_shutdown(m_socket, 0);
	nn_close(m_socket);
}

void IPCClient::updateState(const CarmaState &state)
{
	m_stateLock.lock();
	m_state = state;
	m_hasNewState = true;
	m_stateLock.unlock();
}

CarmaAction IPCClient::getAction()
{
	CarmaAction action;
	m_actionLock.lock();
	action = m_action;
	m_actionLock.unlock();
	
	return action;
}

void IPCClient::IPCStateLoop(IPCClient *client)
{
	if(!client)
		return;

	while(client->m_isRunning) {
		client->m_stateLock.lock();
		if(client->m_hasNewState)
		{
			client->m_hasNewState = false;
			
			libcarma::CarmaState serializedState;
			SerializeState(client->m_state, &serializedState);
			client->m_stateLock.unlock();
			
			//std::cout << "Sending state!" << std::endl;
			std::string str;
			serializedState.SerializeToString(&str);
			nn_send(client->m_socket, str.c_str(), str.size(), 0);
			//std::cout << "State sent!" << std::endl;
		}
		else
		{
			client->m_stateLock.unlock();
		}
	}
}


void IPCClient::IPCActionLoop(IPCClient *client)
{
	if(!client)
		return;
	
	while(client->m_isRunning) {
		//std::cout << "Waiting for action!" << std::endl;
		char *buf = NULL;
		int numBytes = nn_recv (client->m_socket, &buf, NN_MSG, 0);
		assert (numBytes >= 0);
		
		libcarma::CarmaAction serializedAction;
		serializedAction.ParseFromArray(buf, numBytes);
		CarmaAction action;
		DeserializeAction(&serializedAction, action);
		nn_freemsg(buf);
		
		//std::cout << "Received action!" << std::endl;
		
		client->m_actionLock.lock();
		client->m_action = action;
		client->m_actionLock.unlock();
	}
	
}
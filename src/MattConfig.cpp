//
//  MattConfig.cpp
//  vdrift
//
//  Created by Matt Vitelli on 2/1/16.
//  Copyright © 2016 VDrift Team. All rights reserved.
//

#include "MattConfig.hpp"
bool g_drawGUI = false;
bool g_drawMap = false;
bool g_overrideDefaultResolution = false;//true;//true;
int	g_overrideResolutionWidth = 32;
int	g_overrideResolutionHeight = 32;
//
//  MattConfig.hpp
//  vdrift
//
//  Created by Matt Vitelli on 2/1/16.
//  Copyright © 2016 VDrift Team. All rights reserved.
//

#ifndef MattConfig_hpp
#define MattConfig_hpp

#include <stdio.h>

extern bool g_drawGUI;
extern bool g_drawMap;
extern bool g_overrideDefaultResolution;
extern int	g_overrideResolutionWidth;
extern int	g_overrideResolutionHeight;

#endif /* MattConfig_hpp */

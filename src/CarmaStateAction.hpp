//
//  CarmaStateAction.hpp
//  vdrift
//
//  Created by Matt Vitelli on 2/2/16.
//  Copyright © 2016 VDrift Team. All rights reserved.
//

#ifndef CarmaStateAction_hpp
#define CarmaStateAction_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "mathvector.h"

enum class SimulationState
{
	STARTED = 0,
	RUNNING,
	ENDED,
	LOADING
};

class CarmaState
{
public:
	CarmaState();
	~CarmaState();
	
	cv::Mat					m_image;
	Vec3						m_linearVelocity;
	Vec3						m_linearAcceleration;
	Vec3						m_angularVelocity;
	Vec3						m_angularAcceleration;
	SimulationState	m_simulationState;
};

class CarmaAction
{
public:
	CarmaAction();
	~CarmaAction();
	
	float	m_steering;
	float m_acceleration;
	bool	m_requestReset;
};

#endif /* CarmaStateAction_hpp */

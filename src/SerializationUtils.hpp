//
//  SerializationUtils.hpp
//  vdrift
//
//  Created by Matt Vitelli on 2/2/16.
//  Copyright © 2016 VDrift Team. All rights reserved.
//

#ifndef SerializationUtils_hpp
#define SerializationUtils_hpp

#include <stdio.h>
#include "libcarma.pb.h"
#include <opencv2/opencv.hpp>
#include "mathvector.h"
#include "CarmaStateAction.hpp"

void SerializeMatrix(const cv::Mat &inMat, libcarma::SerializedMatrix *outMat);

void DeserializeMatrix(const libcarma::SerializedMatrix *inMat, cv::Mat &outMat);

void SerializeVector(const Vec3 &input, libcarma::SerializedVector3 *output);

void DeserializeVector(const libcarma::SerializedVector3 *input, Vec3 &output);

void DeserializeAction(const libcarma::CarmaAction *input, CarmaAction &output);

void SerializeState(const CarmaState &input, libcarma::CarmaState *output);

//void SerializeVector(

#endif /* SerializationUtils_hpp */
